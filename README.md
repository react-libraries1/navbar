# @verybadcodes-org/navbar

> Made with create-react-library

[![NPM](https://img.shields.io/npm/v/@verybadcodes-org/navbar.svg)](https://www.npmjs.com/package/@verybadcodes-org/navbar) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save @verybadcodes-org/navbar
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from '@verybadcodes-org/navbar'
import '@verybadcodes-org/navbar/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [verybadcodes](https://github.com/verybadcodes)
