import React from 'react'
import logo from './logo.svg';
import { CustomNavbar } from '@verybadcodes-org/navbar'
import '@verybadcodes-org/navbar/dist/index.css'

const App = () => {

  const leftItems = [
    {title: 'nav1', path: 'nav1'},
    {title: 'NavDropdown', subItems:[
      {title: 'navdropdown1', path:'/navdropdown1'}
    ]}
  ]

  return <CustomNavbar logo={logo} leftItems={leftItems} search/>
}

export default App
