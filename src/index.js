import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import styles from './styles.module.css'
import { Button, FormControl, Nav, Navbar, NavDropdown, Form, Image } from 'react-bootstrap';

export const CustomNavbar = (props) => {

  const logoElement = ({logo}) => {
    return logo && <Navbar.Brand href="/" style={{padding: 0}}><Image src={logo} width="60" roundedCircle/></Navbar.Brand>
  }

  const searchBarElement = ({search}) => {
    return search && (
      <Form inline>
          <FormControl type="text" placeholder="Search" className="mr-sm-2" />
          <Button variant="outline-success">Search</Button>
      </Form>
    )
  }

  const subItems = (title, subItems) => {
    return (
      <NavDropdown title={title} id="basic-nav-dropdown">
        {subItems.map((item, key)=>(
          <NavDropdown.Item href={item.path}>{item.title}</NavDropdown.Item>
        ))}
      </NavDropdown>
    )
  }

  const leftItems = ({leftItems}) => {
    return leftItems && leftItems.map((item, key)=>{
      if(item.subItems){
        return subItems(item.title, item.subItems)
      }else{
        return <Nav.Link href={item.path}>{item.title}</Nav.Link>
      }
    })
  }

  return (
    <Navbar bg="light" expand="lg" className="sticky-nav">
      {logoElement(props)}
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          {leftItems(props)}
        </Nav>
        {searchBarElement(props)}
      </Navbar.Collapse>
    </Navbar>
  )
}
